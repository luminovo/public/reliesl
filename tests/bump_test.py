import shutil
import tempfile as tmp
from pathlib import Path

import pytest
from click.testing import CliRunner

import reliesl.cli.bump as bump
from reliesl.cli.bump import _get_date
from reliesl.cli.bump import CHANGELOG
from reliesl.cli.bump import PYPROJECT


TEST_DOC_PATH = Path("tests/assets/test_documents")
RESULT_DOC_PATH = Path("tests/assets/result_documents")
ADDITIONAL_FILE = "additional_file.yaml"
ADDITIONAL_FILE_REGEX = "(lumi_version: )(.*)"


@pytest.fixture()
def bump_setup():
    runner = CliRunner()
    with tmp.TemporaryDirectory() as tmpdir:
        path_tmpdir = Path(tmpdir)
        file_paths = [PYPROJECT, CHANGELOG, Path(ADDITIONAL_FILE)]
        tmp_files = [
            (shutil.copy(TEST_DOC_PATH / file, path_tmpdir / file), file)
            for file in file_paths
        ]
        yield tmp_files, tmpdir, runner


def compare_files(file1: Path, file2: Path):
    with file1.open("r") as f1, file2.open("r") as f2:
        for l1, l2 in zip(f1, f2):
            assert l1 == l2.replace("$CURRENT_DATE$", _get_date())


def test_bump(bump_setup):
    tmp_files, tmpdir, runner = bump_setup
    result = runner.invoke(
        bump,
        args=[
            "--project-dir",
            tmpdir,
            "--additional-file-edits",
            ADDITIONAL_FILE,
            ADDITIONAL_FILE_REGEX,
        ],
        input="2\ny",
    )
    for tmp_file, file in tmp_files:
        compare_files(Path(tmp_file), RESULT_DOC_PATH / file)
    assert not result.exception
    assert result.exit_code == 0


def test_dry_run(bump_setup):
    tmp_files, tmpdir, runner = bump_setup
    result = runner.invoke(
        bump,
        args=[
            "--project-dir",
            tmpdir,
            "--additional-file-edits",
            ADDITIONAL_FILE,
            ADDITIONAL_FILE_REGEX,
            "--dry-run",
        ],
    )
    for tmp_file, file in tmp_files:
        compare_files(Path(tmp_file), TEST_DOC_PATH / file)
    assert not result.exception
    assert result.exit_code == 0
