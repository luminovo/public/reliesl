<p align="center">
<a href="https://github.com/ambv/black"><img alt="Code style: black" src="https://img.shields.io/badge/code%20style-black-000000.svg"></a>
</p>

# reliesl
reliesl helps you automate your releases using `poetry` and GitLab CI.

<div align="center">
    <img src="assets/liesl.jpg" alt="Liesl" width=200px align="middle">
</div>

"You've had a long day at work. Bugs have been squashed, features wered added and breaking changes documented. You've poured your heart and soul into this piece of software and the last thing seperating you from popping a bottle of champagne is having to release the newest version of your project on GitLab. Thank God there's reliesl to release your new software on GitLab (and maybe even open that bottle of champagne)."
\- reliesl's secret admirer

## Use `reliesl`
`reliesl` grew up in a small village and thus is pretty opinionated. It assumes you are using `poetry` and keeping a `CHANGELOG.md`.
`reliesl` provides two different commands to make your release process as smooth as possible:

### reliesl release
Usually added to your existing CI pipeline to manually trigger a new GitLab release based on the releasenotes given in CHANGELOG.md. Also adds the .whl of the package to the Asset section.

If you want to automatically release to PyPi (or TestPyPi if you are feeling less adventurous) you can use the `--pypi-release <pypi|testpypi>` option like so:
```bash
reliesl release --pypi-release <value>
```
For authentication this requires the environement variables  `PYPI_USERNAME`  and `PYPI_PASSWORD` or `TESTPYPI_USERNAME` and `TESTPYPI_PASSWORD` to be set.

### reliesl bump
Usually run locally to take care of all cumbersome version edits in various files. 
It will ask you whether you want to do a pre-release, bug fix, minor or major change, before automatically bumping your `poetry` version and parsing the `CHANGELOG.md` to update the `UNRELEASED` section with the right version number and date.

Use the `--dry-run` flag to see what would happen without changing any files.

Additional files where version changes are needed can be configured via `--additional-file-edits <file> <regex>`, like so: 

```
reliesl bump --additional-file-edits "file.txt" '(semver = )(.*)()' "file2.txt" '(version: )(.*)()'
```

The second capture group must be the semantic version you want to change.
For more information on python regular expressions look [here](https://docs.python.org/3.7/howto/regex.html).

## Install `reliesl`

### Local
Simply run
```bash
poetry add --dev reliesl
```
or 
```bash
pip install reliesl
```

### GitLab-CI
To your .gitlab-ci.yml you need to add:

 - A new `- deploy` stage if you do not already have one (The release process should be the last stage)

 - A new job:
```
release:
  stage: deploy
  when: manual
  script:
    - pip install reliesl
    - reliesl release
```


### Environment Variables
The release script needs access to the GitLab API to create a new release. This requires a new environment variable(Under Settings > CI / CD > Variables):
 * Key: `PRIVATE_TOKEN`
 * Value: a valid private token that gives access to api calls for your project(Profile > Settings > Access Tokens)


## Development

### Pre-commit
Please make sure to have the pre-commit hooks installed.
Install [pre-commit](https://pre-commit.com/) and then run `pre-commit install` to register the hooks with git.

### Makefile
We use [make](https://www.gnu.org/software/make/) to streamline our development workflow.
Run `make help` to see all available commands.

<!-- START makefile-doc -->
```
$ make help 
help                 Show this help message
check                Run all static checks (like pre-commit hooks)
test                 Run all tests
dev-install          Install all the packages in the local python environment for development
bump                 Bump reliesl version 
```
<!-- END makefile-doc -->

## Acknowledgments

The picture of Liesl was stolen from here: https://ladiesoftheatre.tumblr.com/post/155234955388/you-may-think-this-kind-of-adventure-never-may
